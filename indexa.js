/* async await */
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

const { User } = require('./models');

app.use(express.json());


app.get('/users', async (req, res) => {
  try {
    const users = await User.findAll()
    if ( users === null) {
      isSucces = false
      message = 'Users are empty!'
    } 
    res.status(200).json({
      succes: true,
      message: 'Users retrieved!',
      data: users
    })
  } catch (err) {
     res.status(400).json(err)
  }
});


app.get('/users/:id', async (req, res) => {
  try {
    const isSucces = true;
    const message = `Users with ID ${req.params.id} retrieved!`
    const user = await User.findByPk(req.params.id)
    
    if ( user === null ) {
      isSucces = false
      message = `Users with ID ${req.params.id} not found!`
    }
    res.status(200).json({
      succes: isSucces,
      message: message,
      data: user
    })
  } catch(err) {
    res.status(400).json(err)
  }
});

app.post('/users', async (req, res) => {
  try {
    const user = await User.create({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    })
    res.status(201).json({
      succes: true,
      message: `Users created!`,
      data: user 
    })
  } catch (err) {
    res.status(400).json(err)
 }
})


app.put('/users/:id', async (req, res) => {
  try {
    const isSucces = true;
    const message = `Users with ID ${req.params.id} updated!`
    const user = await User.findByPk(req.params.id)

    if ( user === null ) {
      isSucces = false
      message = `Users with ID ${req.params.id} not found!`
    }
    res.status(200).json({
      succes: isSucces,
      message: message,
      data: user
    })
  } catch (err) {
    res.status(400).json(err)
  }
})

app.delete('/users/:id', async (req, res) => {
  try {
    const user = await User.destroy({
      where: {
        id: req.params.id
      }
    })
    res.status(204).end()
  } catch (err) {
    res.status(400).json(err)
 }
})

app.listen(port, () => console.log(`Listening on port ${port}!`))
