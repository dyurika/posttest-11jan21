/* promise */
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

const { User } = require('./models');

app.use(express.json());


app.get('/users', (req, res) => {
  const isSucces = true;
  const message = 'Users retrieved!'
  User.findAll().then(users => {
    if ( users === null) {
      isSucces = false
      message = 'Users are empty!'
    } 
    res.status(200).json({
      succes: isSucces,
      message: message,
      data: { users }
    })
  }).catch((err) => {
     res.status(400).json(err)
  })
});


app.get('/users/:id', (req, res) => {
  const isSucces = true;
  const message = `Users with ID ${req.params.id} retrieved!`
  User.findByPk(req.params.id).then(user => {
    if ( user === null ) {
      isSucces = false
      message = `Users with ID ${req.params.id} not found!`
    }
    res.status(200).json({
      succes: isSucces,
      message: message,
      data: user
    })
  }).catch((err) => {
     res.status(400).json(err)
  })
});

app.post('/users', (req, res) => {
  User.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  }).then(user => {
    res.status(201).json({
      succes: true,
      message: `Users created!`,
      data: user 
    })
  }).catch((err) => {
    res.status(400).json(err)
 })
})


app.put('/users/:id', (req, res) => {
  const isSucces = true;
  const message = `Users with ID ${req.params.id} updated!`

  User.findByPk(req.params.id).then(user => {
    if ( user === null ) {
      isSucces = false
      message = `Users with ID ${req.params.id} not found!`
    }
    user.update({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    }, {
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(200).json({
        succes: isSucces,
        message: message,
        data: user 
      })
    })
  }).catch((err) => {
    res.status(400).json(err)
 })
});

app.delete('/users/:id', (req, res) => {
  User.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(204).end()
  }).catch((err) => {
    res.status(400).json(err)
 })
})

app.listen(port, () => console.log(`Listening on port ${port}!`))
