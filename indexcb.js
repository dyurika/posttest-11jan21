/* callback */
const { request } = require('express');
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

const { User } = require('./models');

app.use(express.json());


app.get('/users', (req, res) => {
  const allUser = (callback) => {
    User.findAll().then(users => {
      if (users !== null ) {
        return callback(null, users)
      }
      return callback(err)
    }).catch((err) => {
      return callback(err)
    })
  }

  allUser ((err, result) => {
    if (!err) {
      res.status(200).json({
        success: true,
        message: `All Users retrieved!`,
        data: result
      })
    } else {
      res.status(422).json({
        sucess: false,
        message: `Users are empty!`,
        data: err
      })
    }
  })
})


    
app.get('/users/:id', (req, res) => {
  const findUser = (id, callback) => {
    User.findByPk(req.params.id).then(user => {
      if( user !== null ){
        return callback(null, user)
      }
      return callback(err)
    }).catch((err) => {
      return callback(err)
      })
  }

  findUser(req.params.id, (err, result) => {
    if (!err) {
      res.status(200).json({
        success: true,
        message: `User with id ${req.params.id} retrieved!`,
        data: result
      })
    } else {
      res.status(422).json({
        sucess: false,
        message: `User with id ${req.params.id} not found!`,
        data: err
      })
    }
  })
})


app.post('/users', (req, res) => {
  const createUser = (data,callback) => {
  if (data !== null) {
    User.create({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
    }).then(user => {
      return callback(null, user)
    }).catch((err) => {
      return callback(err)
    })
  }
    return callback(err)
  }

  createUser(req.body, (err, result) => {
    if (!err) {
      res.status(200).json({
        success: true,
        message: `User created!`,
        data: result})
    } else {
      res.status(422).json({
        sucess: false,
        message: err
      })
    }
  })
})

app.put('/users/:id', (req, res) => {
  const updateUser = (id, callback) => {
    User.findByPk(id).then((user) => {
      if ( user !== null ) {
        user.update({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password
        }, {where: {
          id: req.params.id
        }
        } )
        return callback(null, user)
      }
    }).catch((err) => {
      return callback(err)
    })
  }
  updateUser(req.params.id, (err, result) => {
     if (!err) {
       res.status(200).json({
         success: true,
         message: `User with id ${req.params.id} Updated!`,
         data: result})
     } else {
       res.status(422).json({
         sucess: false,
         message: err
       })
     }
   })
})



app.delete('/users/:id', (req, res) => {
  const deleteUser = (id, callback) => {
    User.destroy({
      where: {
        id: req.params.id
      }
    }).then((user) => {
      return (callback(null, user))
    }).catch((err) => {
      return (callback(err))
    })
  }

  deleteUser(req.params.id, (err, result) => {
    if (!err) {
      res.status(200).json({
        success: true,
        message: `User with id ${req.params.id} Deleted!`,
        data: result})
    } else {
      res.status(422).json({
        sucess: false,
        message: err
      })
    }
  })
})

app.listen(port, () => console.log(`Listening on port ${port}!`))  
